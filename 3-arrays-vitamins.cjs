const items = [{
    name: 'Orange',
    available: true,
    contains: "Vitamin C",
}, {
    name: 'Mango',
    available: true,
    contains: "Vitamin K, Vitamin C",
}, {
    name: 'Pineapple',
    available: true,
    contains: "Vitamin A",
}, {
    name: 'Raspberry',
    available: false,
    contains: "Vitamin B, Vitamin A",
}, {
    name: 'Grapes',
    contains: "Vitamin D",
    available: false,
}];


/*

    1. Get all items that are available 
    2. Get all items containing only Vitamin C.
    3. Get all items containing Vitamin A.
    4. Group items based on the Vitamins that they contain in the following format:
        {
            "Vitamin C": ["Orange", "Mango"],
            "Vitamin K": ["Mango"],
        }
        
        and so on for all items and all Vitamins.
    5. Sort items based on number of Vitamins they contain.

    NOTE: Do not change the name of this file

*/ 
//------------------------------------------------------------------------------

// Solution 1
function availableItems(data)
{
    if(data.available===true)
    {
        return data;
    }
}

let itemList= items.filter(availableItems);

console.log(itemList);


//---------------------------------------------------------------------
console.log("----------------------------------------");
//Solution 2

function vitaminCItems(data)
{
    if(data.contains==="Vitamin C")
    {
        return data;
    }
}

let itemContainOnlyVitaminC= items.filter(vitaminCItems);

console.log(itemContainOnlyVitaminC);

//---------------------------------------------------------------------
console.log("----------------------------------------");
//Solution 3

function vitaminAItems(data)
{
    if(data.contains.includes("Vitamin A"))
    {
        return data;
    }
}

let itemContainVitaminA= items.filter(vitaminAItems);

console.log(itemContainVitaminA);


//---------------------------------------------------------------------
console.log("----------------------------------------");
//Solution 4

let vitaminsObject={};

function groupByVitamins(data)
{
    let itemName=data.name;
    let vitamins=data.contains.split(',');

    vitamins.map(vitaminGroup=>
        {
            if(!vitaminsObject[vitaminGroup.trim()])
            {
                vitaminsObject[vitaminGroup.trim()]=[];
            }
            vitaminsObject[vitaminGroup.trim()].push(itemName);
        });
    //console.log(vitaminsObject);
}

items.map(groupByVitamins);
console.log(vitaminsObject);


//---------------------------------------------------------------------
console.log("----------------------------------------");
//Solution 5

/*

items.sort(( a , b ) => (a.contains.length)-(b.contains.length));

console.log(items);

*/  // working on it